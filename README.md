## Descarga, instalación y dependencias

**Descargas y SDK**. Descargue las versiones más reciente de [Node.js](https://nodejs.org/en/download/current/) (*Latest Features*) y [Git](https://git-scm.com/downloads), y, una vez instalado el primero, desde la línea de comandos, el *framework* Ionic y Cordova como sigue:

```
npm install -g ionic cordova
```

**Nota:** es posible que su sistema le solicite credenciales de administrador.

**Clonar este respositorio**. Este repositorio es privado, entonces, si tiene los permisos suficiente y agregó su *SSH key* (*click* en su avatar, luego *Settings*, *SSH keys* (*Security*) y, finalmente, *Add key*), puede hacer desde el terminal:

```
git clone git@bitbucket.org:josemarroquintoledo/cultural-events-web-cal.git
```

**Dependencias de la aplicación**. El gestor de paquete de JavaScript, npm, descargará e instalará automática las dependencias de la *app*. Entonces, con el siguiente comando, dentro de la carpeta de sus copia local:

```
npm install
```

**Probar la app**. Es posible arrancar un servidor que le permita visualizar la aplicación desde su navegador. Para ello, teclee:

```
ionic serve
```

El servidor intentará visitar de la URL que aparece en la salida en el terminal; si no ocurre, puede hacerlo Ud. maualmente.

## Referencias

Drifty Co. (2017). *Ionic Component Documentation*. Recuperado de https://ionicframework.com/docs/components/