import { Component,
         ViewChild,
         ElementRef } from '@angular/core';
import { NavController,
         Platform,
         Searchbar,
         LoadingController } from 'ionic-angular';
// Providers
import { DataCneProvider } from '../../providers/data-cne/data-cne';
import {LocationServiceProvider} from '../../providers/location-service/location-service';
declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private MARKER_ICONS_PATH = 'assets/markers/';
  // Regions
  private regions: any;
  private lookingForRegions: boolean;
  private items: Array<any>;
  @ViewChild('regionSearchbar') regionSearchbar: Searchbar;
  // Region's communes
  private regionCommunes: any;
  private lookingForRegionCommunes: boolean;
  private subitems: Array<any>;
  private selectedCommune: string;
  private communeCode: string;
  private lastCommune: string;
  private serviceStations: any;
  private cheapestServStations: Array<any>;
  private searching: boolean;
  // Region
  private selectedRegion: string;
  private regionCode: string;
  private lastRegion: string;
  //
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private locating: boolean;
  // Markers
  private myLocationMarker: any;
  private stationMarkers: any;
  private bounds: any;

  constructor(public navCtrl: NavController,
              public dataCneService: DataCneProvider,
              public loadingCtrl: LoadingController,
              public platform: Platform,
              public locationService: LocationServiceProvider) {
    this.cheapestServStations = new Array();
    this.selectedRegion = '';
    this.selectedCommune = '';
    this.lastCommune = '';
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  initializeRegions() {
    this.lookingForRegions = true;
    return new Promise(resolve => {
      this.dataCneService.getRegions().then(res => {
        this.regions = res;
        this.regions.sort((a, b) => {
          if (a.nombre > b.nombre) {
            return 1;
          } else if (a.nombre < b.nombre) {
            return -1;
          }
          return 0;
        });
        this.lookingForRegions = false;
        resolve();
      }).catch(() => {
        this.lookingForRegions = false;
        // Sorry, we can't retrive the Chile's regions.
      });
    });
  }

  initRegionCommunes() {
    this.lookingForRegionCommunes = true;
    return new Promise(resolve => {
      this.dataCneService.getRegionCommunes(this.regionCode).then(res => {
        this.regionCommunes = res;
        this.regionCommunes.sort((a, b) => {
          if (a.nombre > b.nombre) {
            return 1;
          } else if (a.nombre < b.nombre) {
            return -1;
          }
          return 0;
        });
        this.lookingForRegionCommunes = false;
        resolve();
      }).catch(() => {
        console.log("Error: it was impossible to retrieve " + this.regionCode + "'s communes.");
        this.lookingForRegionCommunes = false;
      });
    }); 
  }

  selectRegion(regionObj: any) {
    this.regionCode = regionObj.codigo;
    this.selectedRegion = regionObj['nombre'];
    this.initRegionCommunes().then(() => {
      this.subitems = this.regionCommunes;
    });
  }

  resetCommuneAndRegion() {
    this.selectedCommune = '';
    this.selectedRegion = '';
  }

  initStationMarkers() {
    this.stationMarkers = {
      '93': new google.maps.Marker({
        icon: this.MARKER_ICONS_PATH + '93-marker.png'
      }),
      '95': new google.maps.Marker({
        icon: this.MARKER_ICONS_PATH + '95-marker.png'
      }),
      '97': new google.maps.Marker({
        icon: this.MARKER_ICONS_PATH + '97-marker.png'
      })
    };
  }

  loadMap() {
    this.locating = true;
    this.locationService.getCurrentPosition()
    .then((resp => {
      let myLocation = new google.maps.LatLng(resp.lat,resp.lng);
      let mapOptions = {
        center: myLocation,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.myLocationMarker = new google.maps.Marker({
        position: myLocation,
        icon: {
          strokeColor: '#488AFF',
          path: google.maps.SymbolPath.CIRCLE,
          scale: 10
        }
      });
      this.myLocationMarker.setMap(this.map);
      this.locating = false;
    }))
    .catch((err => { console.log("Can't retrieve the current position.");}))
  }

  getSubitems(ev: any) {
    // Reset items back to all of the items
    if (!this.regionCommunes) {
      // regions (Any) is undefined.
      this.initRegionCommunes().then(() => {
        this.subitems = this.regionCommunes;
        // After obtaining it, do a search.
        this.getSubitems(ev);
      });
    } else {
      this.subitems = this.regionCommunes;

      // set val to the value of the searchbar
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.subitems = this.subitems.filter(subitem => {
          return (subitem.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.resetCommuneAndRegion();
    if (!this.regions) {
      // regions (Any) is undefined.
      this.initializeRegions().then(() => {
        this.items = this.regions;
        // After obtaining it, do a search.
        this.getItems(ev);
      });
    } else {
      this.items = this.regions;

      // set val to the value of the searchbar
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.items = this.items.filter(item => {
          return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    } 
  }

  prepareRegions() {
    this.initializeRegions().then(() => {
      this.items = this.regions;
    });
  }

  getCheapestServStation() {
    this.initStationMarkers();  // Reset station's markers.
    this.bounds = new google.maps.LatLngBounds();  // Reset bounds.
    let threeFuel = 10000;  // 93 octane fuel
    let fiveFuel = 10000;  // 95 octane fuel
    let sevenFuel = 10000;  // 97 octane fuel
    let servStations = {};
    let pos = {};
    for (let station of this.serviceStations.data) {
      if (station.precios['gasolina 93'] && station.precios['gasolina 93'] < threeFuel) {
        threeFuel = station.precios['gasolina 93'];
        servStations['93'] = station;
        pos = {
          lat: servStations['93'].ubicacion.latitud,
          lng: servStations['93'].ubicacion.longitud
        };
        this.stationMarkers['93'].setPosition(pos);
        // Add the marker to the map (alternative method).
        this.stationMarkers['93'].setMap(this.map);
        this.bounds.extend(pos);
      }
      if (station.precios['gasolina 95'] && station.precios['gasolina 95'] < fiveFuel) {
        fiveFuel = station.precios['gasolina 95'];
        servStations['95'] = station;
        pos = {
          lat: servStations['95'].ubicacion.latitud,
          lng: servStations['95'].ubicacion.longitud
        };
        this.stationMarkers['95'].setPosition(pos);
        this.stationMarkers['95'].setMap(this.map);
        this.bounds.extend(pos);
      }
      if (station.precios['gasolina 97'] && station.precios['gasolina 97'] < sevenFuel) {
        sevenFuel = station.precios['gasolina 97'];
        servStations['97'] = station;
        pos = {
          lat: servStations['97'].ubicacion.latitud,
          lng: servStations['97'].ubicacion.longitud
        };
        this.stationMarkers['97'].setPosition(pos);
        this.stationMarkers['97'].setMap(this.map);
        this.bounds.extend(pos);
      }
    }
    this.cheapestServStations = Object.keys(servStations).map(fuelType => {
      return {
        type: fuelType,
        station: servStations[fuelType]
      }
    });
    if (this.cheapestServStations.length > 0) {
      console.log("They were found cheapest service station for: " + Object.keys(servStations));
    } else {
      console.log("Sorry, they weren't found cheapest service station!");
    }
  }
 
  searchForCity(city: any) {
    if (this.cheapestServStations.length > 0) this.cheapestServStations = new Array(); 
    this.lastCommune = '';
    this.searching = true;
    this.communeCode = city.codigo;
    this.selectedCommune = city['nombre'];
    const loading = this.loadingCtrl.create({
      content: "Buscando las bencineras m\u00e1s econ\u00f3micas en " + city['nombre'] + "...",
    });
    loading.present();
    this.dataCneService.getServiceStations(this.regionCode, this.communeCode)
      .then(res => {
        this.serviceStations = res;
        this.getCheapestServStation();
        this.map.fitBounds(this.bounds);  // Do the map shows the bounds.
        loading.dismiss();
        this.searching = false;
        if (this.locating) this.locating = false;
    }).catch(() => {
      // Sorry, we can't retrive the communeCode's service stations.
      loading.dismiss();
      this.searching = false;
      if (this.locating) this.locating = false;
    });
  }
 
  goClosestStation(){
    // UNUSED.
    const loading = this.loadingCtrl.create({
      content: "Buscando la bencinera más cercana ...",
    });
    loading.present();
     this.locationService.getClosestGasStation()
     .then((resp => {
      loading.dismiss();
      let myLocation = new google.maps.LatLng(resp.lat,resp.lng);
      this.map.setCenter(myLocation);
      this.map.setZoom(14);  // Restart the zoom.
      this.myLocationMarker.setPosition(myLocation);
      }))
     .catch((err => {
       console.log("Can't retrieve Gas Station");
     }))
   }

  getCheapestServStsFromWhereIAm() {
    this.locating = true;
    this.locationService.getCurrentPosition().then(res => {
      let pos = res;
      // Reset the array that contains the cheapest stations because I know
      // where I am.
      this.cheapestServStations = new Array();
      this.map.setCenter(pos);
      this.myLocationMarker.setPosition(pos);
      this.myLocationMarker.setMap(this.map);
      this.locationService.getCurrentComune(pos).then(resp => {
        let city = resp;
        this.regionCode = city['codigo_padre'];
        this.selectedRegion = '';  // Reset it because we only have the
                                   // commune's name.
        this.searchForCity(city);
      }).catch(() => {
        console.log("Can't retrieve the current commune.");
      });
    }).catch(err => {
      this.locating = false;
      console.log("Can't retrieve the current position.");
    });
  }

  searchAgain() {
    this.items = new Array();
    this.subitems = new Array();
    this.lastRegion = this.selectedRegion;
    this.lastCommune = this.selectedCommune;
    this.resetCommuneAndRegion();
    setTimeout(() => {
      this.regionSearchbar.setFocus();
    });
  }

  goBack() {
    if (this.lastCommune != '') {
      this.selectedRegion = this.lastRegion;
      this.selectedCommune = this.lastCommune;
      this.lastCommune = '';  // To hide the back button.
    }
  }

  itemOnClear(ev: any) {
    if (this.selectedCommune != '') {
      this.selectedCommune = '';
    }
  }

  setSubitemListMarginTop() {
    if (this.platform.is('ios')) {
      return '44px';
    } else {
      return '49px';
    }
  }

  showStation(st: any) {
    let stPos = this.stationMarkers[st.type].getPosition();
    this.map.setCenter(stPos);
  }
}
