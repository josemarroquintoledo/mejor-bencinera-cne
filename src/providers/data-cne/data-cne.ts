import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataCneProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataCneProvider {
  private CNE_API_SERVICE_STATIONS_URL = 'http://api.cne.cl/v3/combustibles/vehicular/estaciones?token=';
  private CNE_API_TOKEN = 'jEdXB6no9e';
  private MODERN_API_URL = 'https://apis.modernizacion.cl/dpa/';
  private MODERN_API_REGIONS = 'regiones';
  private MODERN_API_COMMUNES = 'comunas';
  constructor(private http: Http) {
    console.log('Hello DataCneProvider Provider');

  }

  getServiceStations(region: string, commune: string) {
    // Returns a promise that if it's resolved gives an array with the
    // service stations of region's commune.
    return new Promise((resolve, reject) => {
      let requestURL = this.CNE_API_SERVICE_STATIONS_URL + this.CNE_API_TOKEN;
      requestURL += "&region=['" + region + "']";
      requestURL += "&comuna=['" + commune + "']";
      this.http.get(requestURL).map(res => res.json()).subscribe(data => {
        console.log("getServiceStations's HTTP response: successful.");
        resolve(data);
      }, error => {
        console.log("getServiceStations's HTTP response: " + error);
        reject();
      });
    })
  }

  getRegions() {
    // Returns a promise that if it's resolved, gives an array with the
    // communes of the region's code (region, string).
    return new Promise((resolve, reject) => {
      let requestURL = this.MODERN_API_URL + this.MODERN_API_REGIONS;
      this.http.get(requestURL).map(res => res.json()).subscribe(data => {
        console.log("getRegions's HTTP response: successful.");
        resolve(data);
      }, error => {
        reject();
      })
    });
  }

  getRegionCommunes(region: string) {
    // Returns a promise that if it's resolved, gives an array with the
    // communes of the region's code (region, string).
    return new Promise((resolve, reject) => {
      let requestURL = this.MODERN_API_URL + this.MODERN_API_REGIONS;
      requestURL += '/' + region + '/comunas';
      this.http.get(requestURL).map(res => res.json()).subscribe(data => {
        console.log("getRegionCommnunes's HTTP response: successful.");
        
        resolve(data);
      }, error => {
        reject();
      })
    });
  }

  getAllCommunes(){
  // Returns a promise that if it's resolved, gives an array with all the
  // communes of Chile.
  return new Promise((resolve,reject) => {

    let requestURL =  this.MODERN_API_URL+
                      this.MODERN_API_COMMUNES;
  
    this.http.get(requestURL)
    .map(res => res.json())
    .subscribe(data => {
      console.log("getAllCommunes's HTTP response: successful.");
      resolve(data);
    },error => { 
      reject();
     })
    })
  }
  getServiceStationsByCommune(commune: string) {
    // Returns a promise that if it's resolved gives an array with the
    // service stations of commune.
    return new Promise((resolve, reject) => {
      let requestURL = this.CNE_API_SERVICE_STATIONS_URL + this.CNE_API_TOKEN;
      requestURL += "&comuna=['" + commune + "']";
      this.http.get(requestURL).map(res => res.json()).subscribe(data => {
        console.log("getServiceStations's HTTP response: successful.");
        resolve(data);
      }, error => {
        console.log("getServiceStations's HTTP response: " + error);
        reject();
      });
    })
  }


getAllComuneIdByPos(){
   // Returns a promise that if it's resolved gives a dictionary with the
    // service stations of commune's position as key and comune's code as value.

  return new Promise<any>((resolve,reject)=>{
    let communeByLoc = [];
    this.getAllCommunes()
    .then((Communes)=>{
     for(let commune in Communes){
      
      let lat = Communes[commune].lat;
      let lng = Communes[commune].lng;
      let location = lat+","+lng;
      let codigo = Communes[commune].codigo;
      
      communeByLoc[location] = codigo;         
     }       
      console.log("getAllComuneIdByPos's response: successful.");
      resolve({CommuneInfo:communeByLoc});
      })
    .catch((error)=>{
      console.log("getAllComuneIdByPos's response: " + error);
      reject();
      });
   })
 }

 
   


}
