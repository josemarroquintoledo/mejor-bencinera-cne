import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { DataCneProvider } from '../../providers/data-cne/data-cne';
/*
  Generated class for the LocationServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationServiceProvider {
  private ComunesIdbyPos;
  constructor(private geolocation: Geolocation,
              private platform :Platform,
              private dataCneService : DataCneProvider) {
   console.log('Hello LocationServiceProvider Provider');

   this.dataCneService.getAllComuneIdByPos()
   .then((data)=>{
     this.ComunesIdbyPos = data;
   })
   .catch((err)=>{
    console.log("comunesIdByPos's initialization failed error: " + err)
   })
  }
 
  getCurrentPosition(): Promise<any>{
   // Returns a promise that if it's resolved, 
  // gives current position.
    return new Promise((resolve, reject) => {
    this.platform.ready().then(() => {
        this.geolocation.getCurrentPosition()
        .then((resp)=>{
        let currentposition = {
                        lat:resp.coords.latitude,
                        lng:resp.coords.longitude
                        }
         resolve(currentposition);               

        }).catch((error)=>{
          console.log(" Can't retrieve location" + error);
          reject();
        });
      }).catch(() =>{
        console.log(" Platform not ready ")
         reject();
      });
    })
  }
  
  calculateDistance(origin: any , destination: any) {
    
      let difLat= origin.lat - destination.lat
      let difLng = origin.lng - destination.lng
      let distance=(difLat**2 + difLng**2)**(1/2)
      
      return distance;
    }
   // Returns a promise that if it's resolved, 
  // gives current position region code.  
 getCurrentRegion(pos:any):Promise<string>{
   return new Promise((resolve, reject) =>{
    let distance = 99999999;
    let origin;
    let auxDistance;
    let destination;
    let coderegion;

      origin = {lat: pos.lat,
        lng: pos.lng};

    this.dataCneService.getRegions()
    .then((regiones)=>{
      for( let region in regiones ){
          let comuneLat = regiones[region].lat;
          let comuneLng = regiones[region].lng
          destination = { lat: comuneLat,
                          lng: comuneLng};
    
          auxDistance= this.calculateDistance(origin,destination)
            if(auxDistance<distance){
              distance = auxDistance ;
              coderegion = regiones[region].codigo
            }
    
      }
      resolve(coderegion);
    })
    .catch((err)=>{
      reject();
      console.log("getRegions's response: failed:"+ err)
    })
  
   })
 }
  getCurrentComune(pos:any):Promise<string>{
    // Returns a promise that if it's resolved, 
    // gives a string with commune's code positions.
    //Buscar una manera mas elegante que 999999
    let communeWhereIAm: any;
    return new Promise((resolve, reject) => {
      let distance = 99999999;
      let coordClosestGasStation;
      let origin;
      let destination;
      let communesIdByPos ;
      let auxDistance;

    communesIdByPos = this.ComunesIdbyPos.CommuneInfo;
    
      origin = {lat: pos.lat,
               lng: pos.lng};
      this.getCurrentRegion(origin).then((code)=>{
      this.dataCneService.getRegionCommunes(code)
      .then((Communes)=>{
            for(let comune in Communes){
              let comuneLat = Communes[comune].lat;
              let comuneLng = Communes[comune].lng
              destination = { lat: comuneLat,
                              lng: comuneLng};
        
              auxDistance= this.calculateDistance(origin,destination)
                if(auxDistance<distance){
                  distance = auxDistance ;
                  coordClosestGasStation = comuneLat+","+comuneLng;
                  communeWhereIAm = Communes[comune];
                }
        
            }
            console.log("getCurrentComune's response: successful.");
            if (communeWhereIAm != undefined) {
              // In the response, we detect that for '05101' (string) has '051'
              // (string) as codigo_padre. THAT IS WRONG.
              //
              // Modify the string type value of the key codigo_padre.
              communeWhereIAm['codigo_padre'] = code;
              resolve(communeWhereIAm);
            } else {
              reject();
            } 
          }).catch((error)=>{ 
            console.log("getAllCommunes's response failed : " + error);
          reject();})
          
        }).catch((error)=>{
          console.log("getAllCommunes's response failed : " + error);
          reject();
        })
  })
}

getClosestGasStation(): Promise<any>{
  
     //Buscar una manera más elegante que 99999999
     // Returns a promise that if it's resolved, 
    // gives position of closest gas station .

      let distance = 99999999;
      let coordClosestGasStation;
      let origin;
      let destination;
      let auxDistance;
  
      return new Promise((resolve,reject)=>{
  
      this.getCurrentPosition().then((pos)=>{
        origin = {lat: pos.lat,
                  lng:pos.lng}

        this.getCurrentComune(origin)
        .then((comuneCod)=>{
          this.dataCneService.getServiceStationsByCommune(comuneCod)
          .then((gasStations)=>{
            let gasStation = gasStations["data"] ;
            
            for(let station in gasStation){
  
              let stationLat = gasStation[station].ubicacion.latitud;
              let stationLng = gasStation[station].ubicacion.longitud
  
              destination = { lat: stationLat,
                              lng: stationLng};
        
              auxDistance= this.calculateDistance(origin,destination)
  
                if(auxDistance<distance){
                  distance = auxDistance ;
                  coordClosestGasStation = {lat: stationLat,
                                            lng: stationLng};
                }
        
            }       
            console.log("getServiceStationsByCommune's response: successful."); 
            resolve(coordClosestGasStation);  

          }).catch((error)=>{
            console.log("getServiceStationsByCommune's response: failed:"+ error);
            reject();
          });
        }).catch((error)=>{
          console.log("getCurrentComune's response: failed:"+ error);
          reject();
        });
      }).catch((error)=>{
        console.log("getCurrentPosition's response: failed:"+ error);
        reject();
      })
    });
   }
    
  
  }


